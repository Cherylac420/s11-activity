package com.zuitt.discussion.repositories;


import com.zuitt.discussion.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// An interface marked as @Repository contains methods for database manipulation
@Repository

// By extending CrudRepository, Post Repository will inherit its pre-defined methods for creating, retrieving, updating, and deleting records
//Post is the datatype
//Object is the data type
public interface PostRepository extends CrudRepository<Post, Object> {
}
