package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    // Create
    void createUser(User user);

    // Retrieve
    Iterable<User> getUsers();

    // Update
    ResponseEntity updateUser(Long id, User user);

    // Delete
    ResponseEntity deleteUser(Long id);

}
