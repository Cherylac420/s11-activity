package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(Post post);

    // We can iterate/loop over the posts in order to display them
    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post post);
}
